<?php

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Elements extends Phalcon\Mvc\User\Component
{

    private $_headerMenu = array(
        'pull-left' => array(
            'index' => array(
                'caption' => 'Home',
                'action' => 'index'
            ),
            'manages' => array(
                'caption' => 'Manage Videos',
                'action' => 'index'
            ),
            'upload' => array(
                'caption' => 'Upload',
                'action' => 'index'
            ),
        ),
        'pull-right' => array(
            'session' => array(
                'caption' => 'Admin Login',
                'action' => 'index'
            ),
        )
    );

    private $_tabs = array(
        'Admin' => array(
            'controller' => 'manages',
            'action' => 'home',
            'any' => false
        ),
        'Temp Videos' => array(
            'controller' => 'tempdata',
            'action' => 'index',
            'any' => false
        ),
        'Manage Videos' => array(
            'controller' => 'manages',
            'action' => 'index',
            'any' => false
        ),
        'Accepted Videos' => array(
            'controller' => 'manages',
            'action' => 'acceptvideos',
            'any' => false
        ),
        'Product' => array(
            'controller' => 'acceptvideos',
            'action' => 'index',
            'any' => false
        ),
        'Categories' => array(
            'controller' => 'categories',
            'action' => 'index',
            'any' => false
        ),
        'All Videos' => array(
            'controller' => 'manages',
            'action' => 'viewall',
            'any' => false
        ),
        'Admin Profile' => array(
            'controller' => 'manages',
            'action' => 'profile',
            'any' => false
        )
    );

    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
    public function getMenu()
    {

        $auth = $this->session->get('auth');
        if ($auth) {
            $this->_headerMenu['pull-right']['session'] = array(
                'caption' => 'Log Out',
                'action' => 'end'
            );
        } else {
            unset($this->_headerMenu['pull-left']['manages']);
        }

        echo '<div class="collapse navbar-collapse">';
        $controllerName = $this->view->getControllerName();
        foreach ($this->_headerMenu as $position => $menu) {
            echo '<ul class="nav navbar-nav ', $position, '">';
            foreach ($menu as $controller => $option) {
                if ($controllerName == $controller) {
                    echo '<li class="active">';
                } else {
                    echo '<li>';
                }
                echo Phalcon\Tag::linkTo($controller.'/'.$option['action'], $option['caption']);
                echo '</li>';
            }
            echo '</ul>';
        }
        echo '</div>';
    }

    public function getTabs()
    {
        $controllerName = $this->view->getControllerName();
        $actionName = $this->view->getActionName();
        echo '<ul class="nav nav-tabs">';
        foreach ($this->_tabs as $caption => $option) {
            if ($option['controller'] == $controllerName && ($option['action'] == $actionName || $option['any'])) {
                echo '<li class="active">';
            } else {
                echo '<li>';
            }
            echo Phalcon\Tag::linkTo($option['controller'].'/'.$option['action'], $caption), '<li>';
        }
        echo '</ul>';
    }
}
