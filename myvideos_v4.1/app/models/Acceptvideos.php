<?php

class Acceptvideos extends Phalcon\Mvc\Model
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $product_types_id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $price;

    /**
     * @var string
     */
    public $active;

    public function initialize()
    {
        $this->belongsTo('categories_id', 'Categories', 'id');
    }

}
