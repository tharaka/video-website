<?php

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;

class AcceptvideosController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Tag::setTitle('Manage your Videos');
        parent::initialize();
    }

    public function indexAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Videos", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $acceptvideos = Acceptvideos::find($parameters);
        if (count($acceptvideos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("acceptvideos/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $acceptvideos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("acceptvideos", $acceptvideos);
    }

    
}
