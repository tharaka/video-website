<?php

use Phalcon\Tag as Tag;
use Phalcon\Flash as Flash;
use Phalcon\Session as Session;

class ManagesController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Tag::setTitle('Manage your Videos');
        parent::initialize();
    }

    public function indexAction()
    {
        //
    }
    public function homeAction()
    {
        //
    }
    
    public function manipulateAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Temps", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Temps::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("manages/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }

    public function viewallAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Temps", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Temps::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any Videos");
            return $this->forward("manages/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 10,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }

    /**
     * Edit the active user profile
     *
     */
    public function profileAction()
    {
        //Get session info
        $auth = $this->session->get('auth');

        //Query the active user
        $user = Admins::findFirst($auth['id']);
        if ($user == false) {
            $this->_forward('manages/index');
        }

        $request = $this->request;

        if (!$request->isPost()) {
            Tag::setDefault('name', $user->name);
            Tag::setDefault('email', $user->email);
            Tag::setDefault('password', $user->password);
        } else {

            $name = $request->getPost('name', 'string');
            $email = $request->getPost('email', 'email');
            $password = $request->getPost('password', 'string');

            $name = strip_tags($name);

            $user->name = $name;
            $user->email = $email;
            $user->password = sha1($password);
            if ($user->save() == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $this->flash->success('Admin profile information was updated successfully');
            }
        }
    }

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Temps", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Temps::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("manages/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }

    public function newAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $title = $request->getPost('title');
            $url = $request->getPost('url');

            preg_match(
                    '/[\\?\\&]v=([^\\?\\&]+)/',
                    $url,
                    $matches
                );
            $id = $matches[1];
            //echo "http://www.youtube.com/embed/".$id;

            $embed = "http://www.youtube.com/embed/$id";


            $video = new Videos();
            $video->title = $title;
            $video->url = $url;
            $video->embed = $embed;

            $video->created_at = new Phalcon\Db\RawValue('now()');
            $video->active = 'Y';
            if ($video->save() == false) {
                foreach ($video->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                return $this->forward("manages/new");
            } else {
                $this->flash->success('Thanks for adding new video!');
                return $this->forward('manages/index');
            }
        }
    }

    public function editAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $videos = Videos::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$videos) {
                $this->flash->error("Video to edit was not found");
                return $this->forward("manages/index");
            }
            $this->view->setVar("id", $videos->id);

            Tag::displayTo("id", $videos->id);
            Tag::displayTo("title", $videos->title);
            Tag::displayTo("url", $videos->url);
        }
    }

    public function acceptAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $temps = Temps::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$temps) {
                $this->flash->error("Video to edit was not found");
                return $this->forward("tempdata/index");
            }
            $this->view->setVar("id", $temps->id);

            Tag::displayTo("id", $temps->id);
            Tag::displayTo("title", $temps->title);
            Tag::displayTo("url", $temps->url);
            Tag::displayTo("url", $temps->url);
            Tag::displayTo("description", $temps->description);
            Tag::displayTo("name", $temps->name);
            Tag::displayTo("email", $temps->email);
        }
    }

    public function createAction()
    {
        
    }

    public function forwordAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            //$id = $request->getPost('id');
            $title = $request->getPost('title');
            $url = $request->getPost('url');
            $description = $request->getPost('description');
            $name = $request->getPost('name');
            $email = $request->getPost('email');

            preg_match('/[\\?\\&]v=([^\\?\\&]+)/',$url,$matches);
            $id1 = $matches[1];
            $embed = "http://www.youtube.com/embed/$id1";
            $thumbnail = "http://img.youtube.com/vi/$id1/maxresdefault.jpg";


            $accepttemps = new Acceptvideos();
            $accepttemps->categories_id = '1';
            $accepttemps->title = $title;
            $accepttemps->url = $url;
            $accepttemps->embed = $embed;
            $accepttemps->thumbnail = $thumbnail;
            $accepttemps->description = $description;
            $accepttemps->name = $name;
            $accepttemps->email = $email;
            
            $accepttemps->created_at = new Phalcon\Db\RawValue('now()');
            $accepttemps->active = 'Y';
            if ($accepttemps->save() == false) {
                foreach ($accepttemps->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                return $this->forward("manages/home");
            } else {     

                $this->flash->success('Thanks for adding new accepttemps!');
                return $this->forward('tempdata/index');
            }
        }    
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("manages/index");
        }

        $id = $this->request->getPost("id", "int");
        $videos = Videos::findFirst("id='$id'");
        if ($videos == false) {
            $this->flash->error("product types does not exist " . $id);

            return $this->forward("manages/index");
        }
        $videos->id = $this->request->getPost("id", "int");
        $videos->title = $this->request->getPost("title", "striptags");
        $videos->url = $this->request->getPost("url", "striptags");

        if (!$videos->save()) {
            foreach ($videos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("manages/edit/" . $videos->id);
        } else {
            $this->flash->success("Product Type was updated successfully");
            return $this->forward("manages/edit");
        }
    }

    public function deleteAction($id)
    {
        $id = $this->filter->sanitize($id, array("int"));

        $videos = Videos::findFirst('id="' . $id . '"');
        if (!$videos) {
            $this->flash->error("Video was not found");

            return $this->forward("manages/index");
        }

        if (!$videos->delete()) {
            foreach ($videos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("manages/search");
        } else {
            $this->flash->success("Vidoe was deleted");
            return $this->forward("manages/search");
        }
    }

    public function acceptvideosAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Acceptvideos", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $acceptvideos = Acceptvideos::find($parameters);
        if (count($acceptvideos) == 0) {
            $this->flash->notice("The search did not find any videos");
            return $this->forward("acceptvideos/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $acceptvideos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("acceptvideos", $acceptvideos);
    }
}
