<?php

class UploadController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Upload');
        parent::initialize();
    }

    public function indexAction()
    {
    	$this->session->conditions = null;
    }

    public function startAction()
    {
    }

    public function uploadAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $title = $request->getPost('title');
            $url = $request->getPost('url');
            $description = $request->getPost('description');
            $name = $request->getPost('name');
            $email = $request->getPost('email');
           

            $video = new Temps();
            $video->title = $title;
            $video->url = $url;
            $video->description = $description;
            $video->name = $name;
            $video->email = $email;



            $video->created_at = new Phalcon\Db\RawValue('now()');
            $video->state = 3;
            if ($video->save() == false) {
                foreach ($video->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $this->flash->success('Thanks for uploading videos');
                return $this->forward('upload');
            }
        }
    }


}
