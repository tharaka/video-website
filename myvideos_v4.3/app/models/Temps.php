<?php

class Temps extends Phalcon\Mvc\Model
{
	/**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $state;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $email;

    public function initialize()
    {
        $this->belongsTo('id', 'Temps', 'id');
    }
}
