<?php use Phalcon\Tag as Tag ?>

<?php echo $this->getContent() ?>

<div align="right">
    <?php echo Tag::linkTo(array("urls/new", "Create Product types", "class" => "btn btn-primary")) ?>
</div>

<?php echo Tag::form(array("urls/search", "autocomplete" => "off")) ?>

<div class="center scaffold">

    <h2>Search product types</h2>


    <div class="clearfix">
        <?php echo Tag::submitButton(array("Search", "class" => "btn btn-primary")) ?></td>
    </div>




</div>
