
{{ content() }}

<div class="hero-unit">
    <h1>Welcome to INVO</h1>
    <p>INVO is a revolutionary application to create invoices online for free.
    Receive online payments from your clients and improve your cash flow</p>
    <p>{{ link_to('session/register', 'Try it for Free &raquo;', 'class': 'btn btn-primary btn-large btn-success') }}</p>
</div>

<div class="row">
    <div class="span4">
        <h2>Manage Invoices Online</h2>
        <p>Create, track and export your invoices online. Automate recurring invoices and design your own invoice using our invoice template and brand it with your business logo. </p>
    </div>
    <div class="span4">
        <h2>Dashboards And Reports</h2>
        <p>Gain critical insights into how your business is doing. See what sells most, who are your top paying customers and the average time your customers take to pay.</p>
    </div>

    <div class="span4">
        <table class="table table-bordered table-striped" align="center">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Url</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if(isset($page->items)){
                    foreach($page->items as $productType){ ?>
                <tr>
                    <td><?php echo $productType->id ?></td>
                    <td><?php echo $productType->title ?></td>
                    <td><?php echo $productType->url ?></td>
                </tr>
            <?php }
                } ?>
            </tbody>
        </table>
    </div>

    <div class="span4">
        <div class="videolist" style="margin-top: 10%;position: absolute;right: 10%;">
              <ul style="list-style: none;">
            <?php
                if(isset($page->items)){
                    foreach($page->items as $productType){ ?>

                <li class="size"style="margin-bottom: 5px;">
                  <a href="#" data-toggle="modal" 
                  data-target="#myModal" 
                  data-theVideo="<?php echo $productType->embed; ?>"
                  data-title="<?php echo $productType->title; ?>"
                  data-description="<?php echo 'description'; ?>">link</a>
                </li>

              <?php
                } }
              ?>

              </ul>
          </div>
    </div>



</div>


                    

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
              <div>
                <iframe width="100%" height="350" src=""></iframe>
              </div>
              <div id="description" style="margin-top:5px"></div>
            </div>
            <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Previous</button>
            <button type="button" class="btn btn-primary">Next</button>
          </div>
        </div>
      </div>
    </div>

    <?php echo $this->tag->javascriptInclude("js/jquery.min.js"); ?>
<script>
      //FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
      function autoPlayYouTubeModal(){
        var trigger = $("body").find('[data-toggle="modal"]');
        trigger.click(function() {

          var theModal = $(this).data( "target" ),
          videoSRC = $(this).attr( "data-theVideo" ), 

          videoTitle = $(this).attr( "data-title" ), //get data video title
          videoDescription = $(this).attr( "data-description" ), //get data video description
          videoSRCauto = videoSRC+"?autoplay=1" ;
          $(theModal+' iframe').attr('src', videoSRCauto);
          $(theModal+' button.close').click(function () {
              $(theModal+' iframe').attr('src', videoSRC);
          });  

          $('#myModalLabel').text(videoTitle);
          $('#description').text(videoDescription);

        });
      }

      $(document).ready(function(){
          autoPlayYouTubeModal();
      });

    </script>
