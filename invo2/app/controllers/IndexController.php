<?php

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Welcome');
        parent::initialize();
    }

    public function indexAction()
    {
        if (!$this->request->isPost()) {
            $this->flash->notice('This is a sample application of the Phalcon PHP Framework.
                Please don\'t provide us any personal information. Thanks');
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $productType = Videos::find($parameters);
        if (count($productType) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $productType,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("productType", $productType);
    }

    
}
