-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2014 at 11:44 AM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.15RC1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `evideosite`
--

-- --------------------------------------------------------

--
-- Table structure for table `acceptvideos`
--

CREATE TABLE IF NOT EXISTS `acceptvideos` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `active` varchar(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `embed` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `category` varchar(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `acceptvideos`
--

INSERT INTO `acceptvideos` (`id`, `active`, `title`, `url`, `description`, `embed`, `thumbnail`, `name`, `email`, `category`) VALUES
(5, 'Y', 'Chocolate Kaalla - SJS ', 'https://www.youtube.com/watch?v=mL5H2hdL9GQ', 'Ekadeka monara kaasi purse ekata enne Dan ful aathaleke mama hidinne Waraduna yamthanaka dan mata hangenne Kellata dandeela rim eken unne Chocolate kaalla mage suukiri battiya mage Oya nolabunoth ane paninawa lokaanthe Issara kiyuwata ane aayeth naa kiyan', 'http://www.youtube.com/embed/mL5H2hdL9GQ', 'http://img.youtube.com/vi/mL5H2hdL9GQ/maxresdefault.jpg', 'Pawan', 'pawan@live.com', '1'),
(6, 'Y', 'Imagine Dragons - Demons (Official) ', 'https://www.youtube.com/watch?v=mWRsgZuwf_8', 'Imagine Dragons performing Demons. Debut album Night Visions available now:', 'http://www.youtube.com/embed/mWRsgZuwf_8', 'http://img.youtube.com/vi/mWRsgZuwf_8/maxresdefault.jpg', 'Saman', 'tharakanilupul@gmail.com', '1'),
(8, 'Y', ' Ellie Goulding - Burn', 'https://www.youtube.com/watch?v=CGyEd0aKWZE', 'Order Ellie music & merchandise here: http://ell.li/EGstoreYT  Listen to the complete Ellie Goulding playlist here: http://ell.li/CompleteYT', 'http://www.youtube.com/embed/CGyEd0aKWZE', 'http://img.youtube.com/vi/CGyEd0aKWZE/maxresdefault.jpg', 'Nimal', 'nimal@live.com', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
