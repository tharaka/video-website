<?php

class LinksController extends \phalcon\Mvc\Controller{

	public function indexAction(){

	}

	public function registerAction(){
		$links = new Links();

		$links->title = $this->request->getPost('title');
		$links->link = $this->request->getPost('link');

        //Store and check for errors
        $success = $links->save();



		
        if ($success) {
            ?>
                <script type="text/javascript">alert("You have Succussfully Insert Links!");</script>
            <?php
            header('location: ../links/index');
        } else {
            echo "Sorry, the following problems were generated: ";
            foreach ($links->getMessages() as $message) {
                echo $message->getMessage(), "<br/>";
                echo Phalcon\Tag::linkTo("index", "home page!");
            }
        }
        
	
        $this->view->disable();
	}

}