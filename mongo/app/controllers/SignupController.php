<?php use Phalcon\Tag; ?>
<?php

class SignupController extends \phalcon\Mvc\Controller{

	public function indexAction(){

	}

	public function registerAction(){

		$robots = new Robots();

		$robots->name = $this->request->getPost('name');
		$robots->age = $this->request->getPost('age');

        //Store and check for errors
        $success = $robots->save();

		
        if ($success) {
            ?>
                <script type="text/javascript">alert("You have Succussfully Registered!");</script>
            <?php
            header('location: ../users/index');
        } else {
            echo "Sorry, the following problems were generated: ";
            foreach ($robots->getMessages() as $message) {
                echo $message->getMessage(), "<br/>";
                echo Phalcon\Tag::linkTo("index", "home page!");
            }
        }
        

        $this->view->disable();
	}

}