<?php

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;

class UrlsController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Tag::setTitle('Manage your products');
        parent::initialize();
    }

    public function indexAction()
    {
        //$this->session->conditions = null;
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Videos", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Videos::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("urls/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Videos", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Videos::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("urls/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }
    public function newAction()
    {
    }

    public function editAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $videos = Videos::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$videos) {
                $this->flash->error("Product type to edit was not found");
                return $this->forward("urls/index");
            }
            $this->view->setVar("id", $videos->id);

            Tag::displayTo("id", $videos->id);
            Tag::displayTo("title", $videos->title);
            Tag::displayTo("url", $videos->url);
        }
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("producttypes/index");
        }

        $producttypes = new ProductTypes();
        $producttypes->id = $this->request->getPost("id", "int");
        $producttypes->name = $this->request->getPost("name");

        $producttypes->name = strip_tags($producttypes->name);

        if (!$producttypes->save()) {
            foreach ($producttypes->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("producttypes/new");
        } else {
            $this->flash->success("Product type was created successfully");
            return $this->forward("producttypes/index");
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("urls/index");
        }

        $id = $this->request->getPost("id", "int");
        $videos = Videos::findFirst("id='$id'");
        if ($videos == false) {
            $this->flash->error("product types does not exist " . $id);

            return $this->forward("urls/index");
        }
        $videos->id = $this->request->getPost("id", "int");
        $videos->title = $this->request->getPost("title", "striptags");
        $videos->url = $this->request->getPost("url", "striptags");

        if (!$videos->save()) {
            foreach ($videos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("urls/edit/" . $videos->id);
        } else {
            $this->flash->success("Product Type was updated successfully");
            return $this->forward("urls/edit");
        }
    }

    public function deleteAction($id)
    {
        $id = $this->filter->sanitize($id, array("int"));

        $videos = Videos::findFirst('id="' . $id . '"');
        if (!$videos) {
            $this->flash->error("product types was not found");

            return $this->forward("urls/index");
        }

        if (!$videos->delete()) {
            foreach ($videos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("urls/search");
        } else {
            $this->flash->success("product types was deleted");
            return $this->forward("urls/index");
        }
    }
}
