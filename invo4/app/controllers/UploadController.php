<?php

class UploadController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Upload');
        parent::initialize();
    }

    public function indexAction()
    {
    	$this->session->conditions = null;
    }

    public function startAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $name = $request->getPost('name');
            $age = $request->getPost('age');


            $people = new Peoples();
            $people->name = $name;
            $people->age = $age;

            $people->created_at = new Phalcon\Db\RawValue('now()');
            $people->active = 'Y';
            if ($people->save() == false) {
                foreach ($people->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $this->flash->success('Thanks for sign-up, please log-in to start generating invoices');
                return $this->forward('index');
            }
        }
    }

    public function uploadAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $title = $request->getPost('title');
            $url = $request->getPost('url');

            preg_match(
                    '/[\\?\\&]v=([^\\?\\&]+)/',
                    $url,
                    $matches
                );
            $id = $matches[1];
            //echo "http://www.youtube.com/embed/".$id;

            $embed = "http://www.youtube.com/embed/$id";


            $video = new Videos();
            $video->title = $title;
            $video->url = $url;
            $video->embed = $embed;

            $video->created_at = new Phalcon\Db\RawValue('now()');
            $video->active = 'Y';
            if ($video->save() == false) {
                foreach ($video->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $this->flash->success('Thanks for upload please log-in to start generating invoices');
                return $this->forward('index');
            }
        }
    }


}
