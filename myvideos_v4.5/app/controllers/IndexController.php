<?php

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Welcome');
        parent::initialize();
    }

    public function indexAction()
    {
        // if (!$this->request->isPost()) {
        //     //$this->flash->notice('This is a video application based on the Phalcon PHP Framework');
        // }

        // $parameters = array();
        // if ($this->persistent->searchParams) {
        //     $parameters = $this->persistent->searchParams;
        // }

        // $acceptvideos = Acceptvideos::find($parameters);
        // if (count($acceptvideos) == 0) {
        //     //$this->flash->notice("The search did not find videos");
        //     //return $this->forward("index");
        // }

        // $paginator = new Phalcon\Paginator\Adapter\Model(array(
        //     "data" => $acceptvideos,
        //     "limit" => 4,
        //     "page" => $numberPage
        // ));
        // $page = $paginator->getPaginate();

        // $this->view->setVar("page", $page);
        // $this->view->setVar("acceptvideos", $acceptvideos);

        $this->persistent->searchParams = null;
        $this->view->setVar("acceptvideos", Acceptvideos::find("active = 'Y'"));
    }
    public function categoryAction(){

        $id = $this->request->getQuery("id", "int");

        //echo $id."hitharaka";
        $this->persistent->searchParams = null;
        
        $this->view->setVar("acceptvideos", Acceptvideos::find("categories_id = $id"));


    }
}
