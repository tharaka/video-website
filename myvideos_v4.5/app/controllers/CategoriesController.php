<?php

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;

class CategoriesController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Tag::setTitle('Manage Video Category');
        parent::initialize();
    }

    public function indexAction()
    {
        $this->session->conditions = null;
        $this->persistent->searchParams = null;
        $this->view->setVar("categories", Categories::find());
    }

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Categories", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $categories = Categories::find($parameters);
        if (count($categories) == 0) {
            $this->flash->notice("The search did not find any category");
            return $this->forward("categories/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $categories,
            "limit" => 10,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("categories", $categories);
    }

    public function newAction()
    {
    }

    public function editAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $categories = Categories::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$categories) {
                $this->flash->error("Category to edit was not found");
                return $this->forward("categories/index");
            }
            $this->view->setVar("id", $categories->id);

            Tag::displayTo("id", $categories->id);
            Tag::displayTo("name", $categories->name);
        }
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("categories/index");
        }

        $categories = new Categories();
        $categories->id = $this->request->getPost("id", "int");
        $categories->name = $this->request->getPost("name");

        $categories->name = strip_tags($categories->name);

        if (!$categories->save()) {
            foreach ($categories->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("categories/new");
        } else {
            $this->flash->success("categories type was created successfully");
            return $this->forward("categories/index");
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("categories/index");
        }

        $id = $this->request->getPost("id", "int");
        $categories = Categories::findFirst("id='$id'");
        if ($categories == false) {
            $this->flash->error("category does not exist " . $id);

            return $this->forward("categories/index");
        }
        $categories->id = $this->request->getPost("id", "int");
        $categories->name = $this->request->getPost("name", "striptags");

        if (!$categories->save()) {
            foreach ($categories->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("categories/edit/" . $categories->id);
        } else {
            $this->flash->success("categories Type was updated successfully");
            return $this->forward("categories/index");
        }
    }

    public function deleteAction($id)
    {
        $id = $this->filter->sanitize($id, array("int"));

        $categories = Categories::findFirst('id="' . $id . '"');
        if (!$categories) {
            $this->flash->error("category types was not found");

            return $this->forward("categories/index");
        }

        if (!$categories->delete()) {
            foreach ($categories->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("categories/search");
        } else {
            $this->flash->success("category types was deleted");
            return $this->forward("categories/index");
        }
    }
}
