<?php

use Phalcon\Tag as Tag;
use Phalcon\Flash as Flash;
use Phalcon\Session as Session;

class ManagesController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Tag::setTitle('Manage your Videos');
        parent::initialize();
    }

    public function indexAction()
    {
        //
    }


    /**
     * Edit the active user profile
     *
     */
    public function profileAction()
    {
        //Get session info
        $auth = $this->session->get('auth');

        //Query the active user
        $user = Admins::findFirst($auth['id']);
        if ($user == false) {
            $this->_forward('manages/index');
        }

        $request = $this->request;

        if (!$request->isPost()) {
            Tag::setDefault('name', $user->name);
            Tag::setDefault('email', $user->email);
            Tag::setDefault('password', '');
        } else {

            $name = $request->getPost('name', 'string');
            $email = $request->getPost('email', 'email');
            $password = $request->getPost('password', 'string');

            $name = strip_tags($name);

            $user->name = $name;
            $user->email = $email;
            $user->password = sha1($password);
            if ($user->save() == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
            } else {
                $this->flash->success('Admin profile information was updated successfully');
            }
        }
    }

}
