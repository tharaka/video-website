<?php

use Phalcon\Tag as Tag;
use Phalcon\Flash as Flash;
use Phalcon\Session as Session;
use Phalcon\Mvc\Model\Criteria;


class TempdataController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Welcome');
        parent::initialize();
    }

    public function indexAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Temps", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Temps::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("tempdata/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Temps", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Temps::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("tempdata/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }

    public function forwardAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $videos = Temps::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$videos) {
                $this->flash->error("Video to edit was not found");
                return $this->forward("tempdata/index");
            }
            $this->view->setVar("id", $videos->id);

            Tag::displayTo("id", $videos->id);
            Tag::displayTo("title", $videos->title);
            Tag::displayTo("url", $videos->url);
            Tag::displayTo("url", $videos->url);
            Tag::displayTo("description", $videos->description);
            Tag::displayTo("name", $videos->name);
            Tag::displayTo("email", $videos->email);
        }
    }

    public function acceptAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            //$id = $request->getPost('id');
            $title = $request->getPost('title');
            $url = $request->getPost('url');
            $description = $request->getPost('description');
            $name = $request->getPost('name');
            $email = $request->getPost('email');

            preg_match('/[\\?\\&]v=([^\\?\\&]+)/',$url,$matches);
            $id1 = $matches[1];
            $embed = "http://www.youtube.com/embed/$id1";
            $thumbnail = "http://img.youtube.com/vi/$id1/maxresdefault.jpg";


            $acceptvideos = new Acceptvideos();
            $acceptvideos->title = $title;
            $acceptvideos->url = $url;
            $acceptvideos->embed = $embed;
            $acceptvideos->thumbnail = $thumbnail;
            $acceptvideos->description = $description;
            $acceptvideos->name = $name;
            $acceptvideos->email = $email;
            $acceptvideos->category = '1';

            $acceptvideos->created_at = new Phalcon\Db\RawValue('now()');
            $acceptvideos->active = 'Y';
            if ($acceptvideos->save() == false) {
                foreach ($acceptvideos->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                return $this->forward("manages/home");
            } else {

                // $videos = Temps::findFirst("id='$id'");
                // if ($videos == false) {
                //     $this->flash->error("Temp video does not exit " . $id);
                //     return $this->forward("tempdata/index");
                // }
                // $videos->state = 2;

                // if (!$videos->save()) {
                //     foreach ($videos->getMessages() as $message) {
                //         $this->flash->error((string) $message);
                //     }
                //     return $this->forward("tempdata/index/" . $videos->id);
                // } else {
                //     //$this->flash->success("Product Type was updated successfully");
                //     $this->flash->success('Thanks for adding new acceptvideos!');
                //     return $this->forward("tempdata/index");
                // }             

                $this->flash->success('Thanks for adding new acceptvideos!');
                return $this->forward('tempdata/index');
            }

        }    
    }
}
