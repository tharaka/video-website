<?php

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;

class UrlsController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Tag::setTitle('Manage your products');
        parent::initialize();
    }

    public function indexAction()
    {
        //$this->session->conditions = null;
    }

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Videos", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $videos = Videos::find($parameters);
        if (count($videos) == 0) {
            $this->flash->notice("The search did not find any product types");
            return $this->forward("urls/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $videos,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("videos", $videos);
    }
    public function newAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $title = $request->getPost('title');
            $url = $request->getPost('url');

            preg_match(
                    '/[\\?\\&]v=([^\\?\\&]+)/',
                    $url,
                    $matches
                );
            $id = $matches[1];
            //echo "http://www.youtube.com/embed/".$id;

            $embed = "http://www.youtube.com/embed/$id";


            $video = new Videos();
            $video->title = $title;
            $video->url = $url;
            $video->embed = $embed;

            $video->created_at = new Phalcon\Db\RawValue('now()');
            $video->active = 'Y';
            if ($video->save() == false) {
                foreach ($video->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                return $this->forward("urls/new");
            } else {
                $this->flash->success('Thanks for adding new video!');
                return $this->forward('urls/index');
            }
        }
    }

    public function editAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $videos = Videos::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$videos) {
                $this->flash->error("Video to edit was not found");
                return $this->forward("urls/index");
            }
            $this->view->setVar("id", $videos->id);

            Tag::displayTo("id", $videos->id);
            Tag::displayTo("title", $videos->title);
            Tag::displayTo("url", $videos->url);
        }
    }

    public function acceptAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $videos = Videos::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$videos) {
                $this->flash->error("Video to edit was not found");
                return $this->forward("urls/index");
            }
            $this->view->setVar("id", $videos->id);

            Tag::displayTo("id", $videos->id);
            Tag::displayTo("title", $videos->title);
            Tag::displayTo("url", $videos->url);
        }
    }

    public function createAction()
    {
        
    }

    public function forwordAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $title = $request->getPost('title');
            $url = $request->getPost('url');

            preg_match('/[\\?\\&]v=([^\\?\\&]+)/',$url,$matches);
            $id = $matches[1];
            $embed = "http://www.youtube.com/embed/$id";
            $thumbnail = "http://img.youtube.com/vi/$id/maxresdefault.jpg";
            

            $acceptvideos = new Acceptvideos();
            $acceptvideos->title = $title;
            $acceptvideos->url = $url;
            $acceptvideos->embed = $embed;
            $acceptvideos->thumbnail = $thumbnail;

            $acceptvideos->created_at = new Phalcon\Db\RawValue('now()');
            $acceptvideos->active = 'Y';
            if ($acceptvideos->save() == false) {
                foreach ($acceptvideos->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                return $this->forward("urls/index");
            } else {
                $this->flash->success('Thanks for adding new acceptvideos!');
                return $this->forward('urls/search');
            }
        }
    }

    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("urls/index");
        }

        $id = $this->request->getPost("id", "int");
        $videos = Videos::findFirst("id='$id'");
        if ($videos == false) {
            $this->flash->error("product types does not exist " . $id);

            return $this->forward("urls/index");
        }
        $videos->id = $this->request->getPost("id", "int");
        $videos->title = $this->request->getPost("title", "striptags");
        $videos->url = $this->request->getPost("url", "striptags");

        if (!$videos->save()) {
            foreach ($videos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("urls/edit/" . $videos->id);
        } else {
            $this->flash->success("Product Type was updated successfully");
            return $this->forward("urls/edit");
        }
    }

    public function deleteAction($id)
    {
        $id = $this->filter->sanitize($id, array("int"));

        $videos = Videos::findFirst('id="' . $id . '"');
        if (!$videos) {
            $this->flash->error("Video was not found");

            return $this->forward("urls/index");
        }

        if (!$videos->delete()) {
            foreach ($videos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("urls/search");
        } else {
            $this->flash->success("Vidoe was deleted");
            return $this->forward("urls/index");
        }
    }
}
