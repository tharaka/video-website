<?php use Phalcon\Tag; ?>
<?php

class UploadController extends \phalcon\Mvc\Controller{

    public function indexAction(){

    }

    public function uploadvideoAction(){

        $basics = new Basics();

        $basics->title = $this->request->getPost('title');
        $basics->url = $this->request->getPost('url');

        $url = $this->request->getPost('url');
        preg_match(
                '/[\\?\\&]v=([^\\?\\&]+)/',
                $url,
                $matches
            );
        $id = $matches[1];
        //echo "http://www.youtube.com/embed/".$id;

        $basics->embed = "http://www.youtube.com/embed/$id";
        $basics->description = $this->request->getPost('description');

        //Store and check for errors
        $success = $basics->save();

        
        if ($success) {
            $this->flash->notice("You have Succussfully Upload the video!");
            return $this->dispatcher->forward(array(
                "controller" => "upload",
                "action" => "index"
            ));
            //header('location: ../upload/index');
        } else {
            echo "Sorry, the following problems were generated: ";
            foreach ($basics->getMessages() as $message) {
                echo $message->getMessage(), "<br/>";
                echo Phalcon\Tag::linkTo("index", "home page!");
            }
        }

        $this->view->disable();
    }

}