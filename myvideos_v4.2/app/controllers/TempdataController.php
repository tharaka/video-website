<?php

use Phalcon\Tag as Tag;
use Phalcon\Flash as Flash;
use Phalcon\Session as Session;
use Phalcon\Mvc\Model\Criteria;


class TempdataController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Phalcon\Tag::setTitle('Welcome');
        parent::initialize();
    }

    public function indexAction()
    {
        $this->persistent->searchParams = null;
        $this->view->setVar("temps", Temps::find());
    }

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Temps", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $temps = Temps::find($parameters);
        if (count($temps) == 0) {
            $this->flash->notice("The search did not find videos");
            return $this->forward("tempdata/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $temps,
            "limit" => 8,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("temps", $temps);
    }

    public function forwardAction($id)
    {

        $request = $this->request;
        if (!$request->isPost()) {

            $temps = Temps::findFirst(array('id=:id:', 'bind' => array('id' => $id)));
            if (!$temps) {
                $this->flash->error("Video to edit was not found");
                return $this->forward("tempdata/index");
            }
            $this->view->setVar("id", $temps->id);

            Tag::displayTo("id", $temps->id);
            Tag::displayTo("title", $temps->title);
            Tag::displayTo("url", $temps->url);
            Tag::displayTo("url", $temps->url);
            Tag::displayTo("description", $temps->description);
            Tag::displayTo("name", $temps->name);
            Tag::displayTo("email", $temps->email);

            $this->persistent->searchParams = null;
            $this->view->setVar("categories", Categories::find());
        }
    }

    public function acceptAction()
    {
        $request = $this->request;
        if ($request->isPost()) {

            $id = $request->getPost("id", "int");
            $title = $request->getPost('title');
            $url = $request->getPost('url');
            $description = $request->getPost('description');
            $name = $request->getPost('name');
            $email = $request->getPost('email');

            preg_match('/[\\?\\&]v=([^\\?\\&]+)/',$url,$matches);
            $id1 = $matches[1];
            $embed = "http://www.youtube.com/embed/$id1";
            $thumbnail = "http://img.youtube.com/vi/$id1/maxresdefault.jpg";


            $acceptvideos = new Acceptvideos();
            $acceptvideos->id = $id;
            $acceptvideos->categories_id = $request->getPost("categories_id", "int");
            $acceptvideos->title = $title;
            $acceptvideos->url = $url;
            $acceptvideos->embed = $embed;
            $acceptvideos->thumbnail = $thumbnail;
            $acceptvideos->description = $description;
            $acceptvideos->name = $name;
            $acceptvideos->email = $email;

            

            
            $acceptvideos->created_at = new Phalcon\Db\RawValue('now()');
            $acceptvideos->active = 'Y';
            if ($acceptvideos->save() == false) {
                foreach ($acceptvideos->getMessages() as $message) {
                    $this->flash->error((string) $message);
                }
                return $this->forward("manages/home");
            } else {     
                $id = $request->getPost("id");
                $temps = Temps::findFirst("id='$id'");
                $temps->state = 1;

                if (!$temps->save()) {
                    foreach ($temps->getMessages() as $message) {
                        $this->flash->error((string) $message);
                    }

                    return $this->forward("acceptvideos/edit/" . $temps->id);
                } else {
                    $this->flash->success("Thanks for adding new acceptvideos!");
                    return $this->forward("tempdata/index");
                }

                //$this->flash->success('Thanks for adding new acceptvideos!');
                //return $this->forward('tempdata/index');
            }

        }    
    }
}
