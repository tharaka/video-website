<?php

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;

class AcceptvideosController extends ControllerBase
{
    public function initialize()
    {
        $this->view->setTemplateAfter('main');
        Tag::setTitle('Manage your Category');
        parent::initialize();
    }

    public function indexAction()
    {
        $this->persistent->searchParams = null;
        $this->view->setVar("categories", Categories::find());
    }

    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Acceptvideos", $_POST);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            if ($numberPage <= 0) {
                $numberPage = 1;
            }
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $acceptvideos = Acceptvideos::find($parameters);
        if (count($acceptvideos) == 0) {
            $this->flash->notice("The search did not find any acceptvideos");
            return $this->forward("acceptvideos/index");
        }

        $paginator = new Phalcon\Paginator\Adapter\Model(array(
            "data" => $acceptvideos,
            "limit" => 5,
            "page" => $numberPage
        ));
        $page = $paginator->getPaginate();

        $this->view->setVar("page", $page);
        $this->view->setVar("acceptvideos", $acceptvideos);
    }

    public function newAction()
    {
        $this->view->setVar("categories", Categories::find());
    }

    public function editAction($id)
    {
        $request = $this->request;
        if (!$request->isPost()) {

            $id = $this->filter->sanitize($id, array("int"));

            $acceptvideos = Acceptvideos::findFirst('id="' . $id . '"');
            if (!$acceptvideos) {
                $this->flash->error("acceptvideos was not found");
                return $this->forward("acceptvideos/index");
            }

            $this->view->setVar("id", $acceptvideos->id);

            Tag::displayTo("id", $acceptvideos->id);
            Tag::displayTo("categories_id", $acceptvideos->categories_id);
            Tag::displayTo("title", $acceptvideos->title);
            Tag::displayTo("description", $acceptvideos->description);
            Tag::displayTo("active", $acceptvideos->active);

            $this->view->setVar("categories", Categories::find());
        }
    }

    public function createAction()
    {

        $request = $this->request;

        if (!$request->isPost()) {
            return $this->forward("acceptvideos/index");
        }

        $acceptvideos = new Acceptvideos();
        $acceptvideos->id = $request->getPost("id", "int");
        $acceptvideos->categories_id = $request->getPost("categories_id", "int");
        $acceptvideos->title = $request->getPost("title");
        $acceptvideos->description = $request->getPost("description");
        $acceptvideos->active = $request->getPost("active");

        if (!$acceptvideos->save()) {

            foreach ($acceptvideos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("acceptvideos/new");

        } else {
            $this->flash->success("acceptvideos was created successfully");
            return $this->forward("acceptvideos/index");
        }
    }

    public function saveAction()
    {
        $request = $this->request;
        if (!$request->isPost()) {
            return $this->forward("acceptvideos/index");
        }

        $id = $request->getPost("id", "int");
        $acceptvideos = Acceptvideos::findFirst("id='$id'");
        if ($acceptvideos == false) {
            $this->flash->error("acceptvideos does not exist ".$id);
            return $this->forward("acceptvideos/index");
        }

        $acceptvideos->id = $request->getPost("id", "int");
        $acceptvideos->categories_id = $request->getPost("categories_id", "int");
        $acceptvideos->title = $request->getPost("title");
        $acceptvideos->description = $request->getPost("description");
        $acceptvideos->active = $request->getPost("active");

        $acceptvideos->name = strip_tags($acceptvideos->name);

        if (!$acceptvideos->save()) {
            foreach ($acceptvideos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }

            return $this->forward("acceptvideos/edit/" . $acceptvideos->id);
        } else {
            $this->flash->success("Video was successfully updated");
            return $this->forward("acceptvideos/index");
        }
    }

    public function deleteAction($id)
    {
        $id = $this->filter->sanitize($id, array("int"));

        $acceptvideos = Acceptvideos::findFirst('id="' . $id . '"');
        if (!$acceptvideos) {
            $this->flash->error("Video was not found");
            return $this->forward("acceptvideos/index");
        }

        if (!$acceptvideos->delete()) {
            foreach ($acceptvideos->getMessages() as $message) {
                $this->flash->error((string) $message);
            }
            return $this->forward("acceptvideos/search");
        } else {
            $this->flash->success("acceptvideos was deleted");
            return $this->forward("acceptvideos/index");
        }
    }
}
